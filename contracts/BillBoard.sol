// SPDX-License-Identifier: MIT
pragma solidity ^0.5.13;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/ownership/Ownable.sol";

contract BillBoard is Ownable {
  bytes32 public message;
  address public stablyTokenContractAddress;
  address public stablyPoolAddress;
  address public messageOwner;
  uint256 public price = 10 * 10**uint256(6);
  
  constructor(
    address _stablyTokenContractAddress,
    address _stablyPoolAddress
  ) public {
    stablyTokenContractAddress = _stablyTokenContractAddress;
    stablyPoolAddress = _stablyPoolAddress;
  }

  function changeStablyTokenContractAddress(address _stably) external onlyOwner {
    require(_stably != address(0), 'Stably address can not equal Address 0');
    stablyTokenContractAddress = _stably;
  }

  function changeStablyPoolAddress(address _pool) external onlyOwner {
    require(_pool != address(0), 'Pool address can not equal Address 0');
    stablyPoolAddress = _pool;
  }

  function changePrice(uint256 _newPrice) external onlyOwner {
    require(_newPrice != 0, 'Price can not equal 0');
    price = _newPrice;
  }

  function setMessage(
    bytes32 _message
  ) external {
    IERC20(stablyTokenContractAddress).transferFrom(msg.sender, stablyPoolAddress, price);
    message = _message;
    messageOwner = msg.sender;
  }
}