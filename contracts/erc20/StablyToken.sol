// SPDX-License-Identifier: MIT
pragma solidity ^0.5.13;

import "@openzeppelin/contracts/token/ERC20/ERC20Detailed.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Mintable.sol";
import "@openzeppelin/contracts/token/ERC20/ERC20Pausable.sol";
import "@openzeppelin/contracts/ownership/Ownable.sol";

/** Upgradeable ERC20 token that is Detailed, Mintable, Pausable */
contract StablyToken is
  Ownable,
  ERC20Detailed,
  ERC20Mintable,
  ERC20Pausable
{
  address _managerContractAddress;
  string constant NAME = "StablyToken";
  string constant SYMBOL = "STA";
  uint8 constant DECIMALS = 6;
  uint256 constant SUPPLY = 1000000 * 10**uint256(DECIMALS);

  constructor() public ERC20Detailed(NAME, SYMBOL, DECIMALS) {
    _mint(msg.sender, SUPPLY);
  }
}
