const Migrations = artifacts.require('Migrations');

const logBytecodes = (contract) => {
  console.log(`${contract.contractName} || bytecode: ${contract.bytecode.length} || deployedBytecode: ${contract.deployedBytecode.length}`);
}

/** log out all contract bytecode lengths */
const StablyToken = artifacts.require('StablyToken');
const BillBoard = artifacts.require('BillBoard');

logBytecodes(StablyToken);
logBytecodes(BillBoard);

module.exports = function(deployer) {
  deployer.deploy(Migrations);
};
