const contractConfig = require('../contract-config.js');
const StablyToken = artifacts.require('StablyToken');


module.exports = (deployer, network, accounts) => {
  deployer.then(async () => {
    const adminAddress = accounts[0] || contractConfig[network].adminAddress;
    const token = await deployer.deploy(StablyToken, { from: adminAddress });
    process.env.tokenAddress = token.address;
  })
}
