const BillBoard = artifacts.require('BillBoard');
const fs = require('fs-extra')
const path = require('path')
const contractConfig = require('../contract-config.js');

module.exports = (deployer, network, accounts) => {
  deployer.then(async () => {
    const adminAddress = accounts[0] || contractConfig[network].adminAddress;
    const stablyPoolAddress = accounts[10] || contractConfig[network].stablyPoolAddress;
    const billboard = await deployer.deploy(
      BillBoard,
      process.env.tokenAddress,
      stablyPoolAddress,
      { from: adminAddress },
    );

    let outputValues = {
      adminAddress,
      tokenAddress: process.env.tokenAddress,
      billboardAddress: billboard.address,
      stablyPoolAddress
    }
    const outputFilePath = path.join(__dirname, 'migration-output.json')
    fs.removeSync(outputFilePath)
    console.log(`Migration output values: ${JSON.stringify(outputValues)}`)
    fs.writeFile(outputFilePath, JSON.stringify(outputValues), (err) => {
      if (err != null) {
        console.log(err)
      }
    })
  })
}
