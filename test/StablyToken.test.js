const StablyToken = artifacts.require("StablyToken");

contract("StablyToken", accounts => {
  it("should appeare right name", () =>
    StablyToken.deployed()
      .then(instance => instance.name.call())
      .then(name => {
        assert.equal(name, 'StablyToken', "name fail");
      })
  );
  it("should appeare right symbol", () =>
    StablyToken.deployed()
      .then(instance => instance.symbol.call())
      .then(symbol => {
        assert.equal(symbol, 'STA', "symbol fail");
      })
  );

  it("should appeare right decimals", () =>
    StablyToken.deployed()
      .then(instance => instance.decimals.call())
      .then(decimals => {
        assert.equal(decimals, 6, "decimals fail");
      })
  );
});