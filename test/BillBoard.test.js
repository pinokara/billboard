const StablyToken = artifacts.require("StablyToken");
const BillBoard = artifacts.require("BillBoard");
const web3 = require('web3');


contract("BillBoard", accounts => {
  const decimal = Math.pow(10, 6);
  it("Initial ", async () => {
    const stably = await StablyToken.new({ from: accounts[0] });
    const billBoard = await BillBoard.new(
      stably.address,
      accounts[10],
      { from: accounts[0] }
    );

    const [
      stablyTokenContractAddress,
      stablyPoolAddress,
      price,
    ] = await Promise.all([
      billBoard.stablyTokenContractAddress(),
      billBoard.stablyPoolAddress(),
      billBoard.price(),
    ]);

    assert.equal(stablyTokenContractAddress, stably.address);
    assert.equal(stablyPoolAddress, accounts[10]);
    assert.equal(price.toNumber(), 10 * decimal);
  });

  it("set another token address, pool address, price", async () => {
    const stably = await StablyToken.new({ from: accounts[0] });
    const billBoard = await BillBoard.new(
      stably.address,
      accounts[10],
      { from: accounts[0] }
    );

    const [
      stablyTokenContractAddress,
      stablyPoolAddress,
      price,
    ] = await Promise.all([
      billBoard.stablyTokenContractAddress(),
      billBoard.stablyPoolAddress(),
      billBoard.price(),
    ]);

    assert.equal(stablyTokenContractAddress, stably.address);
    assert.equal(stablyPoolAddress, accounts[10]);
    assert.equal(price.toNumber(), 10 * decimal);

    await billBoard.changeStablyTokenContractAddress(accounts[2]);
    const stablyTokenContractAddressNew = await billBoard.stablyTokenContractAddress();
    assert.equal(stablyTokenContractAddressNew, accounts[2]);

    await billBoard.changeStablyPoolAddress(accounts[3]);
    const stablyPoolAddressNew = await billBoard.stablyPoolAddress();
    assert.equal(stablyPoolAddressNew, accounts[3]);

    await billBoard.changePrice(20 * decimal);
    const priceNew = await billBoard.price();
    assert.equal(priceNew.toNumber(), 20 * decimal);
    
  });

  it("setMessage ", async () => {
    const stably = await StablyToken.new({ from: accounts[0] });
    const billBoard = await BillBoard.new(
      stably.address,
      accounts[10],
      { from: accounts[0] }
    );
    
    await stably.transfer(accounts[1], 100 * decimal);
    await stably.approve(billBoard.address, 100 * decimal, { from: accounts[1] });
    await billBoard.setMessage(web3.utils.fromAscii('first message'), { from: accounts[1] });

    const message = await billBoard.message();
    assert.equal(web3.utils.toUtf8(message), 'first message');

    const amoutAfterSetMessage = await stably.balanceOf(accounts[1]);
    assert.equal(amoutAfterSetMessage.toNumber(), 90 * decimal);
  });
});